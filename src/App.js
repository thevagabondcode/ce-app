import React from 'react'
import StyledAutoComplete from './components/StyledAutoComplete'
import topics from './topics.json'
import creditType from './creditType.json'

const App = () => {
  return (
    <div>
      <StyledAutoComplete
        inputOptions={topics}
        fuzzyOptions={{
          keys: ['topicName'],
          threshold: 0.5,
          findAllMatches: true,
        }}
        inputLabel="Mandatory Topic(s)"
        optionLabel="topicName"
      />
      <br />
      <StyledAutoComplete
        inputOptions={creditType}
        fuzzyOptions={{
          keys: ['creditType'],
          threshold: 0.5,
          findAllMatches: true,
        }}
        inputLabel="Credit Type(s)"
        optionLabel="creditType"
      />
    </div>
  )
}

export default App
