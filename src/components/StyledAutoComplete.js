import React from 'react'
import Fuse from 'fuse.js'
import slugify from 'slugify'
import Chip from '@material-ui/core/Chip'
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'

const StyledAutoComplete = ({
  inputLabel,
  optionLabel,
  inputOptions,
  fuzzyOptions,
}) => {
  const fuse = new Fuse(inputOptions, fuzzyOptions)

  const filterOptions = (options, { inputValue }) => {
    fuse.search(inputValue)
    const filteredOptions = fuse.search(inputValue)
    const results = filteredOptions.map(({ item }) => item[optionLabel])
    console.log(results)
    return results
  }

  return (
    <Autocomplete
      id={slugify(inputLabel)}
      multiple
      freeSolo
      style={{ width: 600 }}
      options={inputOptions.map((option) => option[optionLabel])}
      filterOptions={filterOptions}
      renderInput={(params) => (
        <TextField {...params} variant="filled" label={inputLabel} />
      )}
      renderTags={(value, getTagProps) =>
        value.map((option, index) => (
          <Chip variant="outlined" label={option} {...getTagProps({ index })} />
        ))
      }
    />
  )
}

export default StyledAutoComplete
